import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as NatsCfg from '../../nats_cfg';
import { ProxiesModule } from '../shared/proxies/proxies.module';
import { DiaryReport } from '../shared/entity/Diary/DiaryReport';
import { MeetingProtocol } from '../shared/entity/Meeting/MeetingProtocol';
import { Project } from '../shared/entity/Project';
import { AppUtilsService } from './app.utils';
import { CONFIG_SCHEMA, CONFIG_FACTORY } from '../configuration';

@Module({
  imports: [
    ConfigModule.forRoot({ load: [CONFIG_FACTORY], validationSchema: CONFIG_SCHEMA }),
    ProxiesModule.register({
      natsHostname: NatsCfg.hostname,
      natsPort: NatsCfg.port,
    }),
    TypeOrmModule.forRoot(),
    TypeOrmModule.forFeature([
      DiaryReport,
      MeetingProtocol,
      Project,
    ]),
  ],
  controllers: [AppController],
  providers: [AppService, AppUtilsService],
})
export class AppModule {}
