import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { MailServiceProxy } from '../shared/proxies/MailServiceProxy';
import { MeetingProtocol } from '../shared/entity/Meeting/MeetingProtocol';
import { DiaryReport } from '../shared/entity/Diary/DiaryReport';
import { Project } from '../shared/entity/Project';
import { RpcException } from '@nestjs/microservices';
import { AppUtilsService, PdfOptions } from './app.utils';
import { VerifiedTokenDto } from '../shared/dto/response/token/verified.token.dto';
import { ActivityStatus } from '../shared/enum/activity/ActivityStatus';
import { DiaryComment } from '../shared/entity/Diary/DiaryComment';
import { DiaryCommentUser } from '../shared/enum/diary/DiaryCommentUser';
import * as fs from 'fs';
import { formatDate } from '../shared/FormatDate';
import { MeetingType } from '../shared/enum/meeting/MeetingType';
import { MailDailyWorkReportSendDto } from '../shared/dto/request/mail_report/mail.daily.report.send.dto';
import { MailProtocolSendDto } from '../shared/dto/request/mail_report/mail.protocol.send.dto';
import { LanguageCode } from '../shared/enum/common/LanguageCode';

@Injectable()
export class AppService {
  constructor(
    private readonly utilsService: AppUtilsService,
    private readonly mailServiceProxy: MailServiceProxy,
    @InjectRepository(MeetingProtocol)
    private readonly meetingProtocolRepository: Repository<MeetingProtocol>,
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    @InjectRepository(DiaryReport)
    private readonly diaryReportRepository: Repository<DiaryReport>,
    private readonly config: ConfigService,
  ) {}

  public async sendDailyDiaryPdfEmail(
    companyId: string,
    projectId: string,
    requestingUser: VerifiedTokenDto,
    diaryReportId: string,
    recipients: string[],
  ) {

    let targetProject: Project;
    try {
      targetProject = await this.projectRepository.findOneOrFail(
        projectId,
        {
          where: {
            removed: false,
          //  ownerCompanyId: companyId,
          },
          relations: ['ownerCompany'],
        },
      );
    } catch (e) {
      console.log(e);
      throw new RpcException('No target project in target company');
    }

    const diaryReport = await this.diaryReportRepository.findOne(
      diaryReportId,
      {
        where: {
          project: targetProject,
        },
        relations: [
          'activityLogs',
          'activityLogs.activity',
          'manpowerEquipmentLogs',
          'manpowerEquipmentLogs.manpowerEquipment',
          'comments',
          'pmSignUser',
          'contractorSignUser',
        ],
      },
    );

    if (!diaryReport) {
      throw new RpcException('No target diary report!');
    }

    const dailyReportNumber = await this.diaryReportRepository
      .createQueryBuilder('diaryReport')
      .where('diaryReport.projectId = :projectId', { projectId: targetProject.id })
      .andWhere('diaryReport.creationDate < :creationDate', { creationDate: diaryReport.creationDate })
      .getCount();

    const renderArgs = {
      // FIXME add function to compare with other rtl languages
      isRtl: (targetProject.languageCode === LanguageCode.he),
      pmSignUser: (diaryReport.pmSignUser && diaryReport.pmSignUser.userName) || '',
      contractorSignUser: (diaryReport.contractorSignUser && diaryReport.contractorSignUser.userName) || '',
      activities: diaryReport.activityLogs.map((al) => {
        return {
          id: al.activity.id,
          name: al.activity.name,
          description: al.description,
          status: this.transformActivityStatus(al.status),
          from: formatDate(al.startDate || al.initialStartDate),
          to: formatDate(al.finishDate || al.initialFinishDate),
        };
      }),
      manpowerEquipments: diaryReport.manpowerEquipmentLogs.map((me) => {
        return {
          group: me.group,
          type: me.type,
          id: me.itemId,
          quantity: me.quantity,
          comment: me.comment,
        };
      }),
      comments: this.transformDiaryComments(diaryReport.comments),
    };

    // important - theese divs use css selectors from outer template file, careful
    const pdfOptions = {
      header: {
        height: '30mm',
        contents:
        `
        <div class="header">
          <p class="header-line-right">Daily work report number: ${dailyReportNumber + 1}</p>
          <p class="header-line-left">Contract number:</p>
          <p class="header-line-right">Date: ${diaryReport.reportDate}</p>
          <p class="header-line-left">Project Name: ${targetProject.name}, ${targetProject.id}</p>
          <p class="header-line-right">Contractor: ${(diaryReport.contractorSignUser && diaryReport.contractorSignUser.userName) || ''}</p>
          <p class="header-line-left">PM: ${(diaryReport.pmSignUser && diaryReport.pmSignUser.userName) || ''}</p>
          <p class="header-line">page {{page}} out of {{pages}}</p>
        </div>
      `,
      },
    } as PdfOptions;

    const { localpath, relativePath } = await this.utilsService.generatePdfFromHtml('diary', renderArgs, pdfOptions);

    const mailDto: MailDailyWorkReportSendDto = {
      company_id: String(targetProject.ownerCompany.id),
      entity_id: String(diaryReport.id),
      project_id: String(targetProject.id),
      system_host: this.config.get('system').host,
      lang_code: LanguageCode.en,
      work_report_number: dailyReportNumber + 1,
      project_name: targetProject.name,
      report_date: String(diaryReport.reportDate),
      pm_name: (diaryReport.pmSignUser && diaryReport.pmSignUser.userName) || '',
      contractor_name: (diaryReport.contractorSignUser && diaryReport.contractorSignUser.userName) || '',
      file_path: relativePath,
    };

    console.log('diary pdf dto to mail service:', mailDto);

    await this.mailServiceProxy.sendDailyReportMail(recipients, mailDto);

    // remove file from fs if email service return success or not
    await fs.promises.unlink(localpath);
  }

  public async sendMeetingPdfEmail(
    companyId: string,
    projectId: string,
    requestingUser: VerifiedTokenDto,
    meetingId: string,
    recipients: string[],
  ) {

    let targetProject: Project;
    try {
      targetProject = await this.projectRepository.findOneOrFail(
        projectId,
        {
          where: {
            removed: false,
          // ownerCompanyId: companyId,
          },
          relations: ['ownerCompany'],
        },
      );
    } catch (e) {
      console.log(e);
      throw new RpcException('No target project in target company');
    }

    const meeting = await this.meetingProtocolRepository.findOne(
      meetingId,
      {
        where: {
          project: targetProject,
        },
        relations: [
          'creator',
          'actionItems',
          'actionItems.category',
          'actionItems.discussions',
          'actionItems.discussions.ownerRole',
          'actionItems.lastDiscussion',
          'actionItems.lastDiscussion.ownerRole',
          'informationItems',
          'informationItems.category',
          'attendees',
          'attendees.existingUser',
          'attendees.existingUser.company',
          'attendees.existingUser.roles',
        ],
      },
    );

    if (!meeting) {
      throw new RpcException('No target meeting protocol!');
    }

    if (meeting.submitted && meeting.pdfReportPath) {
      const mailData: MailProtocolSendDto = {
          project_name: targetProject.name,
          report_date: formatDate(meeting.creationDate).split('-').reverse().join('.'),
          system_host: this.config.get('system').host,
          lang_code: LanguageCode.en,
          meeting_type: meeting.type,
          file_path: meeting.pdfReportPath,
          company_id: String(targetProject.ownerCompany.id),
          project_id: String(targetProject.id),
          meeting_id: String(meeting.id),
      };

      console.log('meeting pdf dto to mail service:', mailData);

      return await this.mailServiceProxy.sendProtocolReportMail(recipients, mailData);
    }

    const renderArgs = {
      // FIXME add function to compare with other rtl languages
      isRtl: (targetProject.languageCode === LanguageCode.he),
      meetingCreatorName: meeting.creator.userName,
      projectName: targetProject.name,
      date: formatDate(meeting.creationDate).split('-').reverse().join('.'),
      meetingType: this.transformMeetingType(meeting.type),
      attendees: meeting.attendees.map((att) => {
        return {
          name: att.name || att.existingUser.userName,
          role: (att.existingUser && att.existingUser.roles.find((r) => r.projectId === meeting.projectId).name) || '',
          company: (att.existingUser && att.existingUser.company && att.existingUser.company.companyName) || '',
        };
      }),

      actionItems: meeting.actionItems.map((ai) => {
        return {
          id: ai.id,
          category: ai.category.name,
          description: {
            background: {
              itemDescription: ai.subject,
              openingDate: formatDate(ai.creationDate),
            },
            comments: ai.discussions.map((d) => {
              return {
                discussionDate: formatDate(d.creationDate),
                commnetorRole: (d.ownerRole && d.ownerRole.name) || '',
                comment: d.discussion,
              };
            }),
          },
          ownerRole: (ai.lastDiscussion.ownerRole && ai.lastDiscussion.ownerRole.name) || '',
          dueDate: formatDate(ai.lastDiscussion.dueDate),
        };
      }),

      informationItems: meeting.informationItems.map((ii) => {
        return {
          id: ii.id,
          category: ii.category.name,
          description: {
            background: {
              openingDate: formatDate(ii.creationDate),
              description: ii.description,
            },
          },
        };
      }),
    };

    // important - theese divs use css selectors from outer template file, careful
    const pdfOptions = {
      header: {
        height: '30mm',
        contents:
        `
        <div class="header">
          <p class="header-line">Date: ${formatDate(meeting.creationDate).split('-').reverse().join('.')}</p>
          <p class="header-line">Protocol ID: ${meeting.id}</p>
          <p class="header-line">Project name and id: ${targetProject.name}, ${targetProject.id}</p>
          <p class="header-line">page {{page}} out of {{pages}}</p>
        </div>
      `,
      },
    } as PdfOptions;

    const { localpath, relativePath } = await this.utilsService.generatePdfFromHtml('meeting', renderArgs, pdfOptions);

    const mailDto: MailProtocolSendDto = {
      project_name: targetProject.name,
      report_date: formatDate(meeting.creationDate).split('-').reverse().join('.'),
      system_host: this.config.get('system').host,
      lang_code: LanguageCode.en,
      meeting_type: meeting.type,
      file_path: relativePath,
      company_id: String(targetProject.ownerCompany.id),
      project_id: String(targetProject.id),
      meeting_id: String(meeting.id),
    };

    console.log('meeting pdf dto to mail service:', mailDto);

    await this.mailServiceProxy.sendProtocolReportMail(recipients, mailDto);

    if (meeting.submitted) {
      await this.meetingProtocolRepository.update(
        meeting.id,
        {
        pdfReportPath: relativePath,
        },
      );
    } else {
      await fs.promises.unlink(localpath);
    }
  }

  private transformActivityStatus(status: ActivityStatus) {
    let readbleStatus: string;
    switch (status) {
      case ActivityStatus.FINISHED: readbleStatus = 'finished'; break;
      case ActivityStatus.ON_HOLD: readbleStatus = 'on hold'; break;
      case ActivityStatus.STARTED: readbleStatus = 'started'; break;
      case ActivityStatus.NOT_STARTED: readbleStatus = 'not started'; break;
      case ActivityStatus.ACHIEVED: readbleStatus = 'achieved'; break;
      case ActivityStatus.NOT_ACHIEVED: readbleStatus = 'not achieved'; break;
      default: throw new RpcException('unknown activity status!');
    }

    return readbleStatus;
  }

  private transformDiaryComments(comments: DiaryComment[]) {
    const pmComments = comments
      .filter((c) => (c.createdBy === DiaryCommentUser.PROJECT_MANAGER) && c.text)
      .sort((a, b) => a.orderId - b.orderId);

    const contractorComments = comments
      .filter((c) => (c.createdBy === DiaryCommentUser.REPORT_CREATOR) && c.text)
      .sort((a, b) => a.orderId - b.orderId);

    const length = pmComments.length >= contractorComments.length
      ? pmComments.length : contractorComments.length;

    const result = [];

    for (let i = 0; i < length; i++) {
      if ((i < pmComments.length) && (i < contractorComments.length)) {
        result.push({ pmComment: pmComments[i].text, contractorComment: contractorComments[i].text });
      } else if (i >= pmComments.length) {
        result.push({ pmComment: '', contractorComment: contractorComments[i].text });
      } else {
        result.push({ pmComment: pmComments[i].text, contractorComment: '' });
      }
    }

    return result;
  }

  private transformMeetingType(meetingType: MeetingType) {
    let result: string;

    switch (meetingType) {
      case MeetingType.CONF_CALL: result = 'conference'; break;
      case MeetingType.WEEKLY_MEETING: result = 'weekly meeting'; break;
      case MeetingType.SPECIAL_MEETING: result = 'special meeting'; break;
    }

    return result;
  }
}
