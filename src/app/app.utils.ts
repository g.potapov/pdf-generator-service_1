import { Injectable } from '@nestjs/common';
import * as PDFGen from 'pdf-creator-node';
import { join } from 'path';
import * as fs from 'fs';
import { v4 as uuidv4 } from 'uuid';
import { RpcException } from '@nestjs/microservices';

export interface PdfOptions {
  header?: {
    height?: string;
    contents: string;
  };
  footer?: {
    height?: string;
    contents: {
      default: string;
      first?: string;
      last?: string;
      [key: string]: string;
    }
  };
}

@Injectable()
export class AppUtilsService {
  private static OUTPUT_BASE_DIR = 'files';
  private static PDF_DIR = 'pdf';
  private static TEMPLATES_DIR = 'templates';
  constructor() {}

  public async generatePdfFromHtml<TRenderArgs>(
    htmlStamp: string,
    renderArgs: TRenderArgs,
    options: PdfOptions,
  ) {
    const file = await fs.promises.readFile(this.formatPath(htmlStamp), {encoding: 'utf8'});
    const fileName = `${uuidv4()}.pdf`;
    await this.checkOrCreateDir(join(__dirname, '..', '..', AppUtilsService.OUTPUT_BASE_DIR));
    const outputDir = join(__dirname, '..', '..', AppUtilsService.OUTPUT_BASE_DIR, AppUtilsService.PDF_DIR);
    const path = join(outputDir, fileName);
    const document = {
      html: file,
      data: renderArgs,
      path,
    };
    const pdfOptions = {
      format: 'A4',
        orientation: 'portrait',
        border: '10mm',
        header: options.header,
        footer: options.footer,
        phantomPath: join(__dirname, '..', '..', 'node_modules', 'phantomjs-prebuilt', 'bin', 'phantomjs'),
    };

    const { filename } = await PDFGen.create(document, pdfOptions);

    return {
      localpath: filename,
      relativePath: join(AppUtilsService.PDF_DIR, fileName),
    };
  }

  private formatPath(stamp: string) {
    return join(__dirname, '..', '..', AppUtilsService.TEMPLATES_DIR, `${stamp}.html`);
  }

  private async checkOrCreateDir(dirPath: string) {
    try {
      await fs.promises.stat(dirPath);
    } catch (e) {
      console.error(e);
      if (e.code && e.code === 'ENOENT') {
        await fs.promises.mkdir(dirPath);
      } else {
        throw new RpcException('error while creating folder');
      }
    }
  }

}
