import { Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';
import { UsersServiceDto } from '../shared/dto/request/base/users-service.dto';
import { UsersServiceUpdateDto } from '../shared/dto/request/base/users-service.update.dto';
import { PDFReportSendDto } from '../shared/dto/request/pdf/pdf.report.send.dto';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
  ) {}

  @MessagePattern({ cmd: 'generate_meeting_pdf' })
  async sendMeetingPdfEmail(
    data: UsersServiceDto<UsersServiceUpdateDto<PDFReportSendDto>>,
  ) {
    console.log('Got generate meetingg pdf request!');
    const companyId = data.companyId;
    const projectId = data.projectId;
    const requestingUser = data.requestingUser;
    const meetingId = data.dto.entityId;
    const recipients = data.dto.dto.recipients;
    return this.appService.sendMeetingPdfEmail(companyId, projectId, requestingUser, meetingId, recipients);
  }

  @MessagePattern({ cmd: 'generate_daily_diary_pdf' })
  async sendDailyDiaryPdfEmail(
    data: UsersServiceDto<UsersServiceUpdateDto<PDFReportSendDto>>,
  ) {
    console.log('Got generate daily diary pdf request!');
    const companyId = data.companyId;
    const projectId = data.projectId;
    const requestingUser = data.requestingUser;
    const diaryReportId = data.dto.entityId;
    const recipients = data.dto.dto.recipients;
    return this.appService.sendDailyDiaryPdfEmail(companyId, projectId, requestingUser, diaryReportId, recipients);
  }
}
