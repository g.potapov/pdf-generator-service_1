import { ArgumentsHost, Catch, ExceptionFilter, BadRequestException, HttpStatus } from '@nestjs/common';
import { throwError } from 'rxjs';
import { ActechError } from '../../shared/errors/ActechError';
import { ActechErrorDescriptor } from '../../shared/enum/errors/ActechErrorDescriptor';

const isEmpty = (obj) => (Object.keys(obj).length === 0 && obj.constructor === Object);

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any, _: ArgumentsHost) {

    if (((exception instanceof ActechError) &&
      exception.error === ActechErrorDescriptor.AUTH_VERIFY_TOKEN_UNAUTHORIZED) === false
    ) {
      console.log('Got exc:', exception);
    }

    let outputException = exception;

    if (exception instanceof BadRequestException) {
      const { message: payload } = exception;
      outputException = new ActechError(
        payload.error,
        HttpStatus.BAD_REQUEST,
        ActechErrorDescriptor.VALIDATION_FORM_ERROR,
        { validationErrors: payload.message },
      );
    } else if (!(exception instanceof ActechError)) {
      const { message, ...payload } = exception;
      // TODO recursive enumerate to reassigned inner property stack if needed
      if (payload.stack) {
        payload.stack = undefined;
      }
      outputException = new ActechError(
        message,
        HttpStatus.INTERNAL_SERVER_ERROR,
        ActechErrorDescriptor.UNKNOWN_INTERNAL_ERROR,
        isEmpty(payload) ? undefined : payload,
      );
    }

    outputException.stack = undefined;

    return throwError(outputException);
  }
}
