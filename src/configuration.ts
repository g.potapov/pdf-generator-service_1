import * as Joi from '@hapi/joi';

export const CONFIG_FACTORY = () => ({
  system: {
    host: process.env.ACTECH_HOST,
  },
});

export const CONFIG_SCHEMA = Joi.object({
  ACTECH_HOST: Joi.string().required(),
});
