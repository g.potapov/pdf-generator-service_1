import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { ValidationPipe } from '@nestjs/common';
import { Transport } from '@nestjs/microservices';
import * as NatsCfg from '../nats_cfg';
import { DtoTransformPipe } from './shared/DtoTransform.pipe';
// import { AppService } from './app/app.service';
import { AllExceptionsFilter } from './app/exceptions/all.exceptions.filter';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(
    AppModule,
    {
      transport: Transport.NATS,
      options: {
        url: `nats://${NatsCfg.hostname}:${NatsCfg.port}`,
      },
    });
  app.useGlobalPipes(
    new DtoTransformPipe(),
    new ValidationPipe({
      transform: true,
      validationError: {
        target: false,
        value: true,
      },
    }),
  );
  app.useGlobalFilters(new AllExceptionsFilter());
  await app.listen(() => console.log('Microservice started!'));

  // just test, remove later
 // await app.get(AppService).sendDailyDiaryPdfEmail('62', '104', {} as any, '227', []);
 // await app.get(AppService).sendMeetingPdfEmail('62', '104', {} as any, '236', []);
}
bootstrap();
