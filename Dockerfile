FROM node:12.7-alpine as build
ENV NPM_CONFIG_LOGLEVEL warn

#RUN apk add --update \
#    python \
#    make \
#    g++ \
#  && rm -rf /var/cache/apk/*

#RUN npm set registry https://infra-nexus-01.alx.altarix.ru/repository/yarnpkg-proxy
#FROM registry.docker.altarix.org:5001/actech/users-service/node:12.7-alpine as build
RUN npm install --unsafe-perm=true typescript@3.6.4 -g

COPY /package.json /package.json
COPY /nats_cfg.js /nats_cfg.js
COPY /src /src
COPY /pm2.json /pm2.json
COPY /tsconfig.json /tsconfig.json
COPY /tsconfig.build.json /tsconfig.build.json
COPY /tslint.json /tslint.json
COPY /nest-cli.json /nest-cli.json
COPY /ormconfig.json /ormconfig.json
COPY /templates /templates


RUN npm install
RUN npm run build

FROM keymetrics/pm2:12-slim

RUN apt-get update \
    && apt-get install -y libfontconfig

COPY --from=build /dist /app/dist
COPY --from=build /node_modules /app/node_modules
COPY --from=build /pm2.json /app/pm2.json
COPY --from=build /package.json /app/package.json
COPY --from=build /ormconfig.json /app/ormconfig.json
COPY --from=build /nats_cfg.js /app/nats_cfg.js
COPY --from=build /templates /app/templates

VOLUME [ "/app/files" ]

WORKDIR /app

CMD [ "pm2-runtime", "start", "pm2.json" ]
