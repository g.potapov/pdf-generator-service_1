module.exports = {
  port: 4222,
  name: "USERS_SERVICE",
  retryAttempts: 5,
  retryDelay: 3000,
  hostname: "nats"
};
